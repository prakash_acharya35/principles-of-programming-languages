c 		program for finding the largest value
c	 X	attained by a set of numbers
	 	DIMENSION A(5)
	 	DATA A /23.1, 12.1, 42.3, 11.5, 12.4/
	 	N=5
	 	I=1
	 	BIGA=A(I)
5 		DO 20 I=2,N
30 		IF(BIGA-A(I)) 10,20,20
10 		BIGA=A(I)
20		CONTINUE
		PRINT 2,N,BIGA
2		FORMAT(22H THE LARGEST OF THESE I3, 12H NUMBERS IS F7.2)
		STOP 77777
		END	